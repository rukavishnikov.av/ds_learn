# https://pyprog.pro/linear_algebra_functions/linalg_lstsq.html
import numpy as np
from numpy import linalg as LA
import matplotlib.pyplot as plt


x = np.arange(10)
y = 2*x + 4 + 2*np.random.randn(10)

print(x)
print(y)
# рисуем зависимость x,y
# plt.scatter(x, y)
# plt.show()

# вычисялем y = XB, где X - это массив вида [[1,x[0]], [1,x[1]], ..., [1,x[n]]], а B = [[B0], [B1]]
X = np.vstack([np.ones(len(x)),x]).T

B = np.linalg.lstsq(X, y, rcond=None)[0] # это вычисленные коэффициенты бетта1 и бетта 2 с крышечкой

print(B)

plt.scatter(x, y)
plt.plot(x, B[0] + B[1]*x, 'r')
plt.show()
