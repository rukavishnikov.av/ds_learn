import urllib
from urllib import request
import numpy as np

# e = np.ones((3, 1))
# X = np.array([60, 50, 75]).reshape(3, 1)
# Y = np.array([10, 7, 12]).reshape(3, 1)
# X = np.append(e, X[:, 0, np.newaxis], axis=1)
# print(X)
# print(Y)
#
#
# B = ((np.linalg.inv(X.T @ X)) @ X.T) @ Y
# print(B)
#

# fname = input()  # read file name from stdin
# f = urllib.request.urlopen(fname)  # open file from URL
f = '/home/ra/Downloads/boston_houses.csv'
data = np.loadtxt(f, delimiter=',', skiprows=1)  # load data to work with
Y = data[:, 0] #get first column
X = np.copy(data) #copy data
X[:, 0] = 1 #change first column to ones
B = ((np.linalg.inv(X.T @ X)) @ X.T) @ Y # calculate B coefficient

print(*B)
