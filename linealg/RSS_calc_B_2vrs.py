import numpy as np
import matplotlib.pyplot as plt

# results -3.655804285060753 -0.21639550236914 0.07373059817548988 4.412450576912782 -25.46844878410154 7.1432015507460624 -1.3010876776491835
f = '/home/ra/Downloads/boston_houses.csv'
data = np.loadtxt(f, delimiter=',', skiprows=1)  # load data to work with
Y = data[:, 0] #get first column
X = np.copy(data) #copy data
X[:, 0] = 1 #change first column to ones
#B = ((np.linalg.inv(X.T @ X)) @ X.T) @ Y # calculate B coefficient
#print(*B)

B = np.linalg.lstsq(X, Y, rcond=None)
print(*B)
