import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

data = pd.read_csv('states.csv')

grid = sns.PairGrid(data)
grid.map(plt.scatter)
plt.show()
