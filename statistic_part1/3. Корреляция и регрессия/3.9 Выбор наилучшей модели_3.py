import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

data = pd.read_csv('states.csv')

features = data.columns
f, ax = plt.subplots(1, 1)
sns.heatmap(data[features].corr(), annot=True, square=True, cmap='coolwarm')
plt.show()
plt.close()
