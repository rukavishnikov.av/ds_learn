from scipy.stats import linregress
import matplotlib.pyplot as plt
import pandas as pd

data = pd.read_csv('states.csv', sep=',')
slope, intercept, r_value, p_value, std_err = linregress(data['hs_grad'], data['poverty'])
plt.title('Модель линейной регрессии')
plt.xlabel('Среднее Образование')
plt.ylabel('Бедность')
plt.plot(data['hs_grad'], data['poverty'], 'o', markersize='3', label='Данные')
plt.plot(data['hs_grad'], intercept + slope * data['hs_grad'], 'r', label='Прямая регрессии')
plt.legend()

print('slope %f\nintercept %f\nr_value %f\np_value %f\nstd_err %f' % (slope, intercept, r_value, p_value, std_err))

plt.show()