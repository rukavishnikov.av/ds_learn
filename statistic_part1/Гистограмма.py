import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import numpy as np

mu, sigma = 100, 15
x = mu + sigma * np.random.randn(1000000)

fig, ax = plt.subplots()
n, bins, patches = ax.hist(x, 50, facecolor='green', alpha=0.75)

ax.yaxis.set_major_formatter(ticker.FuncFormatter(lambda y, pos: '%.0f' % (y * 1e-3)))
ax.set_ylabel('Frequency (000s)')

plt.show()