import numpy as np
import matplotlib.pyplot as plt

data_to_plot = np.array([157,159,161,164,165,166,167,167,167,168,169,170,170,170,171,171,172,172,172,172,173,173,175,175,177,178,178,179,185])
plt.boxplot(data_to_plot)
plt.scatter(np.ones_like(data_to_plot),data_to_plot)
plt.show()