import pandas as pd
import scipy.stats as stats
import seaborn as sns
import matplotlib.pyplot as plt

data = pd.read_csv('genetherapy.csv')
A = data[data["Therapy"] == "A"]["expr"]
B = data[data["Therapy"] == "B"]["expr"]
C = data[data["Therapy"] == "C"]["expr"]
D = data[data["Therapy"] == "D"]["expr"]

# применяем дисперсионный анализ
print(stats.f_oneway(A, B, C, D))
#Получается результат:
#F_onewayResult(statistic=8.0373024811439908, pvalue=0.00015249722895229536)

# Находим среднее для всех терапий
print(data.groupby('Therapy')['expr'].mean())
# sns.set(font_scale=1.2)
# sns.set(rc={'figure.figsize':(10,9)})
ax = (sns.boxplot(x='Therapy', y='expr', data=data)).set(xlabel='group', ylabel='values of experiment', title='group dist')
plt.show()

