# аналогичную задачу можно посмотреть на сайте https://gallery.shinyapps.io/dist_calc/

import scipy.stats as st
# среднее значение, стандартное отклонение
m, sd, xi = 100, 15, 70

# норминуем по z наше выбранное значение xi
# zi = (xi-m)/sd

# оцениваем какой процент значений лежит выше (правее) значения zi
percent1 = 1 - st.norm.cdf((xi-m)/sd)
xi = 112
percent2 = 1 - st.norm.cdf((xi-m)/sd)

print(percent1, percent2, percent1-percent2)