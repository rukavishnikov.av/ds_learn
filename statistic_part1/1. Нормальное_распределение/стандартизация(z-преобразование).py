import numpy as np
import matplotlib.pyplot as plt
import scipy.stats as st

x = np.random.randn(1000) # generate samples from normal distribution (discrete data)
norm_cdf = st.norm.cdf(x) # calculate the cdf - also discrete

# plot the cdf
plt.plot(x, norm_cdf)
plt.show()