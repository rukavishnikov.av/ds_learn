import numpy as np, scipy.stats as st


def p_value(z_stat, alternative='two-sided'):
    if alternative == 'two-sided':
        return 2 * (1 - st.norm.cdf(np.abs(z_stat)))

    if alternative == 'less':
        return st.norm.cdf(z_stat)

    if alternative == 'greater':
        return 1 - st.norm.cdf(z_stat)


x = 115
x_new = 118
sd = 9
n = 144

se = sd / n**0.5
z = (x_new - x) / se

print(p_value(z))
